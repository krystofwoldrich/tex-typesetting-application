import * as React from 'react';
import { Grid, Paper, createStyles, WithStyles, withStyles } from '@material-ui/core';
import AppBar from '../components/AppBar';
import DocumentArtifactsList from '../components/DocumentArtifactsList';
import ArtifactDetail from '../components/ArtifactDetail';

const styles = createStyles({
	gridContainer: {
		height: '100%'
	},
	leftPanel: {
		height: "100%"
	},
	rightPanel: {
		height: '100%'
	}
});

interface EditScreenProps extends WithStyles<typeof styles> {
}

const EditScreen: React.FunctionComponent<EditScreenProps> = ({ classes }: EditScreenProps) => {
	return (
		<React.Fragment>
			<AppBar></AppBar>
			<Grid container spacing={0} className={classes.gridContainer}>
				<Grid item xs={3}>
					<Paper className={classes.leftPanel}>
						<DocumentArtifactsList></DocumentArtifactsList>
					</Paper>
				</Grid>
				<Grid item xs={6}>
				</Grid>
				<Grid item xs={3}>
					<Paper className={classes.rightPanel}>
						<ArtifactDetail></ArtifactDetail>
					</Paper>
				</Grid>
			</Grid>
		</React.Fragment>
	);
};

export default withStyles(styles)(EditScreen);