import * as React from 'react';
import { withStyles, WithStyles, createStyles } from '@material-ui/core/styles';
import {
	List,
	ListItem,
	ListItemAvatar,
	Avatar,
	ListItemText,
	ListItemSecondaryAction,
	IconButton,
	Collapse,
	ListItemIcon
} from '@material-ui/core';
import StarBorder from '@material-ui/icons/StarBorder';
import FolderIcon from '@material-ui/icons/Folder';
import ExpandLess from '@material-ui/icons/ExpandLess';

const styles = createStyles({
	demo: {
		minWidth: '248px'
	},
	nested: {
		paddingLeft: '32px'
	}
}
);

interface DocumentArtifactsListProps extends WithStyles<typeof styles> {
}

const DocumentArtifactsList: React.FunctionComponent<DocumentArtifactsListProps> = ({ classes }: DocumentArtifactsListProps) => {
	return <div className={classes.demo}>
		<List>
			<ListItem>
				<ListItemAvatar>
					<Avatar>
						<FolderIcon />
					</Avatar>
				</ListItemAvatar>
				<ListItemText
					primary="Single-line item"
				/* secondary={secondary ? 'Secondary text' : null} */
				/>
				<ListItemSecondaryAction>
					<IconButton aria-label="Delete">
						<ExpandLess />
					</IconButton>
				</ListItemSecondaryAction>
			</ListItem>
			<Collapse in={true} timeout="auto" unmountOnExit>
				<List component="section" disablePadding>
					<ListItem button className={classes.nested}>
						<ListItemIcon>
							<StarBorder />
						</ListItemIcon>
						<ListItemText inset primary="Starred" />
					</ListItem>
					<ListItem button className={classes.nested}>
						<ListItemIcon>
							<StarBorder />
						</ListItemIcon>
						<ListItemText inset primary="Starred" />
					</ListItem>
					<ListItem button className={classes.nested}>
						<ListItemIcon>
							<StarBorder />
						</ListItemIcon>
						<ListItemText inset primary="Starred" />
					</ListItem>
				</List>
			</Collapse>
		</List>
	</div>;
};

export default withStyles(styles)(DocumentArtifactsList);