import React from 'react';
import './App.css';
import EditScreen from './screens/EditScreen';

interface AppProps {

}

const App: React.FunctionComponent<AppProps> = ({ }: AppProps) => {
  return (
    <React.Fragment>
      <EditScreen></EditScreen>
    </React.Fragment>
  );
}

export default App;
